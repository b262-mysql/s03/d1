-- [SECTION] CRUD Operations

-- Inserting records (Create)

-- To insert an artist in the artist table

INSERT INTO artists (name) VALUES ("Blackpink");
INSERT INTO artists (name) VALUES ("Rivermaya");

-- to insert albums in the albums table
INSERT INTO albums (album_title, data_release, artists_id) VALUES ("The Album", "2020-10-02", 1);
INSERT INTO albums (album_title, data_release, artists_id) VALUES ("Trip", "1996-01-01", 1);


-- To insert songs in the song table
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Ice Cream", 256, "K-pop", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("You Never Know", 239, "K-pop", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 234, "OPM", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 279, "OPM", 2);


-- Read Operations / Retrieving Records

-- Display/Retrieve the title and genre of all the songs.
SELECT song_name, genre FROM songs;

-- Display/Retrieve all the fields from songs table
SELECT * FROM songs;

-- Display/Retrieve the title of all the OPM songs
SELECT song_name FROM songs WHERE genre = 'OPM';

-- Display/Retrieve the title and lenght of the K-pop songs  that are more than 4:00 minutes
SELECT song_name, length FROM songs WHERE length > 400 AND genre = 'Kpop';


-- Updating Records

-- Update the length of You Never Know to 00:04:00.
-- UPDATE <table_name> SET <field_name> = <value> WHERE <validationField = <value>
UPDATE songs SET length = 400 WHERE song_name = "You Never Know";

-- Deleting Records

-- Delete all K-pop songs that are more than 4:00 mins
DELETE FROM songs WHERE genre = "kpop" AND length > 400;

-- Removing the WHERE clause will delete all rows
DELETE FROM songs;